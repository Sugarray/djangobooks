from critic.forms import ReviewForm
from django.views.generic.edit import FormView
from django.views.generic import TemplateView


class ReviewView(FormView):
    template_name = 'bootstrap_review.html'
    form_class = ReviewForm
    success_url = '/critic/thanks/'

    def form_valid(self, form):
        form.send_email()
        return super(ReviewView, self).form_valid(form)


class ThanksView(TemplateView):
    template_name = 'thanks.html'