from django.conf.urls import url
from critic.views import ReviewView, ThanksView

__author__ = 'Vladimir Volodavets'


urlpatterns = [
    url(r'^review/', ReviewView.as_view(), name='review-book'),
    url(r'^thanks/', ThanksView.as_view()),
]
