from django.views.generic import TemplateView
from reader.actions import get_books, get_authors


class HomeView(TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['authors'] = get_authors()
        context['books'] = get_books()
        return context