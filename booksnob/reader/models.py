from django.db import models


class Author(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    email = models.EmailField()

    def __unicode__(self):
        return '{0} {1}'.format(self.first_name, self.last_name)


class Book(models.Model):
    author = models.ForeignKey(Author)  # relationship 1:n
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return str(self.name)